
variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-west-1"
}

variable "access_key"{
description ="Access Key"
}

variable "secret_key"{
  description="Secret Key"
}

variable "clustername"{

description ="Cluster Name"
default ="TestCluster"
}


