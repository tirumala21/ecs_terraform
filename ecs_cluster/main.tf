provider "aws" {
  
  region     = var.aws_region
  access_key = var.access_key 
  secret_key = var.secret_key

}

module "ecs_module"{
   
   source="./ecs_module"
   clustername= var.clustername
   

}
