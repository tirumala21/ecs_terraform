provider "aws" {
  
  region     = var.aws_region
  access_key = var.access_key 
  secret_key = var.secret_key

}

module "ecs_module"{
   source="./ecs_module"
   clustername= var.clustername
   cpucount=    var.cpucount
   memory=      var.memory
   family=      var.family
   container_environment= var.container_environment
   container_image=       var.container_image
   container_name=        var.container_name
   container_port=        var.container_port
   servicename=           var.servicename
   taskcountdesired=      var.taskcountdesired
   minhealth=             var.minhealth
   maxhealth=             var.maxhealth
   albname=               var.albname
   targetgroupname=       var.targetgroupname
   vpc_id=                var.vpc_id
   subnets=               var.subnets
   max_capacity=          var.max_capacity
   min_capacity=          var.min_capacity
   as_memory_targetval=   var.as_memory_targetval
   as_cpu_targetval=      var.as_cpu_targetval


}
