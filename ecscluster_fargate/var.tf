# variables.tf

variable "aws_region" {
  description = "The AWS region things are created in"
  default     = "us-west-1"
}

variable "access_key"{

}

variable "secret_key"{
  
}

variable "clustername"{

}


variable "cpucount"{

}
 
variable  "memory"{

}

variable "family"{

}

variable "container_environment"{

}
   
variable "container_image"{

   }
variable "container_name"{
}

variable "container_port"{

}

variable  "servicename"{
   }  
   
variable "taskcountdesired"{

}
 
variable  "minhealth"{

}
 
variable  "maxhealth"{

}

variable "albname"{

}

variable "targetgroupname"{

}
 
 variable  "vpc_id"{

 }


variable "max_capacity" {
  
}

variable "min_capacity" {
  
}

variable "as_memory_targetval" {
  
}

variable "as_cpu_targetval" {
  
}

variable "subnets"{
  type=list(string)
}