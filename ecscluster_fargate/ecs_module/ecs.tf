


################################ ECS CLUSTER #####################################

resource "aws_ecs_cluster" "ecs" {
  name = "${var.clustername}"
}

################################## TASK DEFINITION ###################################
resource "aws_ecs_task_definition" "task" {
  family                   = var.family
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.cpucount
  memory                   = var.memory 
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  container_definitions = jsonencode([
   
    {
   name        = "${var.container_name}"
   image       = "${var.container_image}:latest"
   essential   = true
   portMappings = [
     {
     protocol      = "tcp"
     containerPort = var.container_port
     hostPort      = var.container_port
   }
   ]


  }])

}


 

################################## SERVICE ##############################################

resource "aws_ecs_service" "service"{
 name                               = "${var.servicename}"
 cluster                            = aws_ecs_cluster.ecs.id
 task_definition                    = aws_ecs_task_definition.task.id
 desired_count                      = var.taskcountdesired
 deployment_minimum_healthy_percent = var.minhealth
 deployment_maximum_percent         = var.maxhealth
 launch_type                        = "FARGATE"
 scheduling_strategy                = "REPLICA"
 
 network_configuration {
  # security_groups  = var.ecs_service_security_groups
   security_groups  = [aws_security_group.securityfortargets.id]
   subnets          = var.subnets####################
   assign_public_ip = true
 }
 
 load_balancer {
   target_group_arn = aws_alb_target_group.targetgroup.id
   container_name   = "${var.container_name}"
   container_port   = var.container_port
 }
 
  depends_on = [aws_alb_listener.http, aws_alb_target_group.targetgroup,aws_iam_role_policy_attachment.ecs_task_execution_role]
}

################################# IAM ROLES ###############################

# ECS task execution role data
data "aws_iam_policy_document" "ecs_task_execution_role" {
  version = "2012-10-17"
  statement {
    sid = ""
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# ECS task execution role
resource "aws_iam_role" "ecs_task_execution_role" {
  name               = var.ecs_task_execution_role_name
  assume_role_policy = data.aws_iam_policy_document.ecs_task_execution_role.json
}

# ECS task execution role policy attachment
resource "aws_iam_role_policy_attachment" "ecs_task_execution_role" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"

}


################################## ALB #######################################

resource "aws_lb" "alb" {
  name               = "${var.albname}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.securityforlb.id]
  subnets            = var.subnets
 
  enable_deletion_protection = false
}
 
 ########################### TARGETGROUPS #####################################

resource "aws_alb_target_group" "targetgroup" {
  name        = var.targetgroupname
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"
 
  health_check {
   healthy_threshold   = "3"
   interval            = "30"
   protocol            = "HTTP"
   matcher             = "200"
   timeout             = "3"
   path                = var.health_check_path
   unhealthy_threshold = "2"
  }
  
    depends_on = [aws_lb.alb]
      
    
  
}
########################## ALB LISTNER HTTP ####################################
resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_lb.alb.id
  port              = 80
  protocol          = "HTTP"
 
  default_action {
    target_group_arn = aws_alb_target_group.targetgroup.id
    type             = "forward"
  }



}
 
 



resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var. max_capacity  
  min_capacity       = var. min_capacity  
  resource_id        = "service/${aws_ecs_cluster.ecs.name}/${aws_ecs_service.service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

########################## AUTOSCALING POLICY (MEMORY & CPU) #############################

resource "aws_appautoscaling_policy" "ecs_policy_memory" {
  name               = "memory-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace
 
  target_tracking_scaling_policy_configuration {
   predefined_metric_specification {
     predefined_metric_type = "ECSServiceAverageMemoryUtilization"
   }
 
   target_value       = var.as_memory_targetval
  }
}
 
resource "aws_appautoscaling_policy" "ecs_policy_cpu" {
  name               = "cpu-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace
 
  target_tracking_scaling_policy_configuration {
   predefined_metric_specification {
     predefined_metric_type = "ECSServiceAverageCPUUtilization"
   }
 
   target_value       = var.as_cpu_targetval
  }
}

########################## SECURITY FOR TARGETS ##############################


resource "aws_security_group" "securityfortargets" {
  name        = "ecs-tasks-security-group"
  description = "allow inbound access from the ALB only"
  vpc_id      = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = var.container_port
    to_port         = var.container_port
    security_groups = [aws_security_group.securityforlb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

########################### SECURITY FOR LOADBALANCER ########################



resource "aws_security_group" "securityforlb" {
  name        = "myapp-load-balancer-security-group"
  description = "controls access to the ALB"
  vpc_id      = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = var.container_port
    to_port     = var.container_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
