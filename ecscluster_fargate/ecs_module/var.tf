variable "clustername" {

}

variable "cpucount" {
  
}

variable "memory" {
  
}

variable "family" {
  
}
variable "container_name" {
  
}

variable "container_image" {
  
}

variable "container_port" {
  
}

variable "container_environment" {
  
}


variable "servicename" {
  
}

variable "taskcountdesired" {
 
}

variable "minhealth" {
  
}

variable "maxhealth" {
  
}


variable "albname" {
  
}

variable "targetgroupname"{

}

variable "vpc_id" {
  
}


variable "subnets" {
type=list(string)
}


variable "health_check_path" {
  default = "/"
}


variable "max_capacity" {
  
}

variable "min_capacity" {
  
}

variable "as_memory_targetval" {
  
}

variable "as_cpu_targetval" {
  
}
variable "ecs_task_execution_role_name" {
  description = "ECS task execution role name"
  default = "myEcsTaskExecutionRole"
}

